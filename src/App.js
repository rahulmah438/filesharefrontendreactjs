import React, { Component } from "react";
import "../src/css/app.css";
import Home from "./pages/home";
import View from "./pages/view";
import { history } from "./history";
import { Router, Route, Switch } from "react-router-dom";
import TopBar from "./components/top-bar";
import LoginForm from "./components/login";

// import { history } from "history";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showLoginForm: false
    };
  }
  render() {
    const { showLoginForm } = this.state;

    return (
      <div className="app-layout">
        <TopBar
          onShowLoginForm={() => {
            this.setState({
              showLoginForm: true
            });
          }}
        />
        {showLoginForm ? (
          <LoginForm
            onClose={() => {
              this.setState({
                showLoginForm: false
              });
            }}
          />
        ) : null}
        <Router history={history}>
          <Switch>
            <Route exact path={"/share/:id"} component={View} />
            <Route exact path={"/"} component={Home} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
